import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:methodchannel/methodchannel.dart';

void main() {
  const MethodChannel channel = MethodChannel('methodchannel');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await Methodchannel.platformVersion, '42');
  });
}
